package com.app.invitame.definition;


public enum ApplicationRoles {

    APPLICATION_USER,
    CONDOMINO,
    RESIDENTE,
    ADMINISTRADOR,
    VIGILANTE;

}
