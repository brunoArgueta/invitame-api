package com.app.invitame;

import com.app.invitame.definition.ErrorCodes;
import lombok.Getter;

public class InvitameException extends Exception {

    @Getter
    private ErrorCodes error;

    public InvitameException(ErrorCodes error) {
        super(error.getMessage());
        this.error = error;
    }


    public InvitameException(Throwable cause, ErrorCodes error) {
        super(error.getMessage(), cause);
        this.error = error;
    }


    public InvitameException(Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCodes error) {
        super(error.getMessage(), cause, enableSuppression, writableStackTrace);
        this.error = error;
    }
}
