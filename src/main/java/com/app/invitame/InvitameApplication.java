package com.app.invitame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaAuditing
@EntityScan("com.app.invitame.entity")
@EnableJpaRepositories(basePackages="com.app.invitame.repository")
@EnableSwagger2
public class InvitameApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvitameApplication.class, args);
	}

}
