package com.app.invitame;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class InvitameKieServices {

    KieServices kieServices;
    KieContainer kieContainer;

    @Bean("InvitameKieServices")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public InvitameKieServices getInvitameKieServices() {
        return new InvitameKieServices();
    }

    public void initializeRules() {
       this.kieServices = KieServices.Factory.get();
       this.kieContainer = this.kieServices.getKieClasspathContainer();
    }

}
