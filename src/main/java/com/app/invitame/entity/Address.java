package com.app.invitame.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="address")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address extends TimedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long address_id;

    @Column(name = "street_name", nullable = false)
    private String streetName;

    @Column(name = "number", nullable = false)
    private Integer number;

    @ManyToOne
    @JoinColumn(name = "complex_id")
    private Complex complex;

    @ManyToMany(mappedBy = "addresses")
    private Set<Account> accounts;

}
