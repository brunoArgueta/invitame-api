package com.app.invitame.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="complex")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Complex extends TimedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long complex_id;

    @Column(name = "complex_name", nullable = false)
    private String complexName;

    @Column(name = "complex_cp", nullable = false)
    private String complexCP;

    @Column(name = "complex_state", nullable = false)
    private String complexState;

    @Column(name = "complex_municipality", nullable = false)
    private String complexMunicipality;

    @OneToMany(mappedBy = "complex")
    private Set<Address> addresses;
}
