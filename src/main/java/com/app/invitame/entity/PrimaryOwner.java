package com.app.invitame.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="primary_owner")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrimaryOwner extends TimedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long primary_owner_id;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @OneToMany(cascade = ALL, mappedBy = "primaryOwner")
    private Set<SecondaryOwner> secondaryOwners;

}
