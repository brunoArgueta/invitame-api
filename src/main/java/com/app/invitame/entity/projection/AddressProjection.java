package com.app.invitame.entity.projection;

import com.app.invitame.entity.Address;
import org.springframework.data.rest.core.config.Projection;

@Projection(
        name = "addressProjection",
        types = {Address.class}
)
public interface AddressProjection {

    String getStreetName();
    Integer getNumber();

}
