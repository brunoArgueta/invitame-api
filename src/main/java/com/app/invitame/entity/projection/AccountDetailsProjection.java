package com.app.invitame.entity.projection;

import com.app.invitame.entity.Account;
import com.app.invitame.entity.ApplicationRole;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(
        name = "accountDetailsProjection",
        types = {Account.class, ApplicationRole.class}
)
public interface AccountDetailsProjection {

    String getFirstName();
    String getLastName();
    String getPhone();
    String getEmail();
    ApplicationRoleProjection getApplicationRole();

    Boolean getAccountNonExpired();
    Boolean getAccountNonLocked();
    Boolean getCredentialsNonExpired();
    Boolean getEnabled();
    Date getAccountExpirationDate();
    Date getAccountLockedDate();
    Date getAccountEnabledDate();

    interface ApplicationRoleProjection{
        String getRoleName();
    }

}
