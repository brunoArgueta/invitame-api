package com.app.invitame.entity.projection;


import com.app.invitame.entity.Address;
import com.app.invitame.entity.Complex;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(
        name = "fullAddressProjection",
        types = {Complex.class}
)
public interface FullComplexProjection {

    Long getComplex_id();

    String getComplexName();

    String getComplexCP();

    String getComplexState();

    String getComplexMunicipality();

    List<AddressProjection> getAddresses();

    @Projection(
            name = "addressProjection",
            types = {Address.class}
    )
    interface AddressProjection{

        Long getAddress_id();

        String getStreetName();

        Integer getNumber();
    }

}
