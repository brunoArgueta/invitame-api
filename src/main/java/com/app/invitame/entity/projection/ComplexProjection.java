package com.app.invitame.entity.projection;


import com.app.invitame.entity.Complex;
import org.springframework.data.rest.core.config.Projection;

@Projection(
        name = "complexProjection",
        types = {Complex.class}
)
public interface ComplexProjection {

    Long getComplex_id();

    String getComplexName();

    String getComplexCP();

    String getComplexState();

    String getComplexMunicipality();

}
