package com.app.invitame.entity;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Entity
@Table(name="account")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account extends TimedEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "account_non_expired", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean accountNonExpired;

    @Column(name = "account_non_locked", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean accountNonLocked;

    @Column(name = "credentials_non_expired", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean credentialsNonExpired;

    @Column(name = "enabled", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean enabled;

    @Column(name = "account_expiration_date")
    private Date accountExpirationDate;

    @Column(name = "account_locked_date")
    private Date accountLockedDate;

    @Column(name = "account_enabled_date")
    private Date accountEnabledDate;

    @ManyToOne
    @JoinColumn(name = "application_role_id",
            foreignKey = @ForeignKey(name = "APPLICATION_ROLE_FK")
    )
    private ApplicationRole applicationRole;

    @ManyToMany
    @JoinTable(name = "account_address",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "address_id")})
    private Set<Address> addresses;

    public void addAddress(Address address){
        if (null == this.addresses){
            this.addresses = new HashSet<>();
        }
        this.addresses.add(address);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<ApplicationRole> applicationRoles = new ArrayList<>();
        applicationRoles.add(this.applicationRole);

        return applicationRoles.stream().map(ApplicationRole::getRoleNames).collect(toList())
                        .stream().map(SimpleGrantedAuthority::new).collect(toList());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
