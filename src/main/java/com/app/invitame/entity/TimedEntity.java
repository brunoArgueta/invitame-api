package com.app.invitame.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public abstract class TimedEntity implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "create_date", nullable = false, updatable = false)
    @CreatedDate
    protected Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "update_date", nullable = false, updatable = false)
    @LastModifiedDate
    protected Date updateDate;

}
