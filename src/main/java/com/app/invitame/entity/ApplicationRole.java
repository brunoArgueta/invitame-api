package com.app.invitame.entity;

import com.app.invitame.definition.ApplicationRoles;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "application_role")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationRole extends TimedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "application_role_id")
    private Long applicationRoleId;

    @Enumerated(EnumType.STRING)
    @Column(name = "role_name")
    private ApplicationRoles roleName;

    @Column(name = "description")
    private String description;

    public String getRoleNames(){
        return this.roleName.name();
    }

}
