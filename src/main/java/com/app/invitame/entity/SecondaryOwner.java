package com.app.invitame.entity;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name="secundary_owner")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecondaryOwner extends TimedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long secondary_owner_id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "primary_owner_id", nullable = false)
    private PrimaryOwner primaryOwner;

}

