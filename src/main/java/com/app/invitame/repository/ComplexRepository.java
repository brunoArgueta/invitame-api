package com.app.invitame.repository;

import com.app.invitame.entity.Complex;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RepositoryRestResource(path = "complex")
public interface ComplexRepository extends CrudRepository<Complex, Long> {

    List<Complex> findAll(Sort sort);

    Optional<Complex> findComplexByComplexName(String complexName);

    @Query(value = "SELECT c FROM Complex c INNER JOIN  Address a WHERE c.complexName = ?1 GROUP BY a.streetName")
    ArrayList<Complex> findStreetNameByComplexName(@Param("complexName") String complexName, Sort sort);

}