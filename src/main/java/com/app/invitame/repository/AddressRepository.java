package com.app.invitame.repository;

import com.app.invitame.entity.Address;
import com.app.invitame.entity.Complex;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;


@RepositoryRestResource(path = "address")
public interface AddressRepository extends CrudRepository<Address, Long> {

    @Query("SELECT a FROM Address a WHERE a.complex = ?1")
    Set<Address> findAllByComplex(Complex complex, Sort sort);

    @Query("SELECT a FROM Address a WHERE a.complex = ?1 and a.streetName = ?2")
    Set<Address> findAllByComplexAndStreetName(Complex complex, String streetName, Sort sort);

    /**
     * Find the Address for a given Complex, Street and Number.
     * @param complexName
     * @param streetName
     * @param number
     * @return Related Address
     */
    @Query("SELECT a FROM Address a WHERE a.complex.complexName = ?1 and a.streetName = ?2 and a.number = ?3")
    Address findAddressByComplexStreetAndNumber(String complexName, String streetName, Integer number);
}
