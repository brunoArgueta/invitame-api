package com.app.invitame.repository;

import com.app.invitame.definition.ApplicationRoles;
import com.app.invitame.entity.ApplicationRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;


@RepositoryRestResource(path = "application_role")
public interface ApplicationRoleRepository extends CrudRepository<ApplicationRole, Long> {

    @Query("SELECT a FROM ApplicationRole a WHERE a.roleName = ?1")
    Optional<ApplicationRole> findByRoleName(ApplicationRoles roleName);
}

