package com.app.invitame.repository;

import com.app.invitame.entity.Account;
import com.app.invitame.entity.projection.AccountDetailsProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.core.userdetails.UserDetailsService;

@RepositoryRestResource(path = "account")
public interface AccountRepository extends CrudRepository<Account, Long>, UserDetailsService {

    /**
     * Find an Account by its email where the email is UNIQUE and it works as username.
     * @param email
     * @return
     */
    Account findByEmail(String email);

    AccountDetailsProjection findAccountByEmail(String email);

    @Query("SELECT a FROM Account a WHERE a.email = ?1")
    Account loadUserByUsername(String email);
}
