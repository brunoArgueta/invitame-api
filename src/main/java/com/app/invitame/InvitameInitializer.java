package com.app.invitame;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class InvitameInitializer {

    private final InvitameKieServices invitameKieServices;

    @PostConstruct
    public void init() {
        this.invitameKieServices.initializeRules();
    }

}
