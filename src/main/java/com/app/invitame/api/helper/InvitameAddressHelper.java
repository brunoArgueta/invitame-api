package com.app.invitame.api.helper;

import com.app.invitame.InvitameException;
import com.app.invitame.definition.ErrorCodes;
import com.app.invitame.entity.Address;
import com.app.invitame.entity.Complex;
import com.app.invitame.repository.AddressRepository;
import com.app.invitame.repository.ComplexRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class InvitameAddressHelper {

    @Autowired
    private ComplexRepository complexRepository;

    @Autowired
    private AddressRepository addressRepository;

    public List<String> getSteetsByComplexName(String complexName) throws InvitameException {
        List<String> ret;
        Optional<Complex> optionalComplex = null;
        Set<Address> addressSet;
        try {
            optionalComplex = complexRepository.findComplexByComplexName(complexName);
            if (optionalComplex.isPresent()){
                addressSet = addressRepository.findAllByComplex(optionalComplex.get(), new Sort(Sort.Direction.ASC, "streetName"));
                ret = addressSet.stream()
                        .map(Address::getStreetName)
                        .distinct()
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList());
            } else {
                return null;
            }
        } catch (Exception ex){
            throw new InvitameException(ex, ErrorCodes.INVITAME_GENERIC_ERROR);
        }
        return ret;
    }


    public List<Integer> getNumbersByComplexNameAndStreetName(String complexName, String streetName) throws InvitameException {
        List<Integer> ret;
        Optional<Complex> optionalComplex;
        try {
            optionalComplex = complexRepository.findComplexByComplexName(complexName);
            if (optionalComplex.isPresent()){
                ret = addressRepository.findAllByComplexAndStreetName(optionalComplex.get(), streetName, new Sort(Sort.Direction.ASC, "streetName"))
                        .stream()
                        .map(Address::getNumber)
                        .distinct()
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList());
            } else {
                return null;
            }
        } catch (Exception ex){
            throw new InvitameException(ex, ErrorCodes.INVITAME_GENERIC_ERROR);
        }
        return ret;
    }
}
