package com.app.invitame.api.helper;

import com.app.invitame.api.reponse.ComplexStreetResponse;
import com.app.invitame.entity.Address;
import com.app.invitame.entity.Complex;
import com.app.invitame.repository.AddressRepository;
import com.app.invitame.repository.ComplexRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ComplexHelper {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ComplexRepository complexRepository;

    public ComplexStreetResponse getSteetsByComplexName(String complexName) {
        Complex complex;
        List<String> address;
        Set<Address> addressSet;


        Optional<Complex> complexOptional = complexRepository.findComplexByComplexName(complexName);

        if (complexOptional.isPresent()){
            complex = complexOptional.get();
            addressSet = addressRepository.findAllByComplex(complex, new Sort(Sort.Direction.ASC, "streetName"));

            address = addressSet.stream()
                    .map(Address::getStreetName)
                    .distinct()
                    .sorted(Comparator.naturalOrder())
                    .collect(Collectors.toList());

            return ComplexStreetResponse.builder()
                    .streetNames(address)
                    .complexCP(complex.getComplexCP())
                    .complexId(complex.getComplex_id())
                    .complexMunicipality(complex.getComplexMunicipality())
                    .complexName(complex.getComplexName())
                    .complexState(complex.getComplexState())
                    .build();
        } else {
            return new ComplexStreetResponse();
        }
    }

}
