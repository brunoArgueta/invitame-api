package com.app.invitame.api.helper;

import com.app.invitame.InvitameException;
import com.app.invitame.api.reponse.SigninResponse;
import com.app.invitame.api.request.SigninRequest;
import com.app.invitame.api.security.EncryptionServiceImpl;
import com.app.invitame.definition.ApplicationRoles;
import com.app.invitame.definition.ErrorCodes;
import com.app.invitame.entity.Account;
import com.app.invitame.entity.Address;
import com.app.invitame.entity.ApplicationRole;
import com.app.invitame.entity.projection.AccountDetailsProjection;
import com.app.invitame.repository.AccountRepository;
import com.app.invitame.repository.AddressRepository;
import com.app.invitame.repository.ApplicationRoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.graalvm.compiler.core.common.type.ArithmeticOpTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Setter
@Log4j
public class InvitameAccountHelper {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ApplicationRoleRepository applicationRoleRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private EncryptionServiceImpl encryptionService;


    /**
     * Creates a new application user as resident
     *
     * @param request
     * @return
     * @throws InvitameException
     */
    public SigninResponse createNewResidentAccount(SigninRequest request) throws InvitameException {
        Account account;
        SigninResponse response;
        String encryptedPassword;
        Set<Address> addressSet;

        ApplicationRole appRole;
        List<SigninRequest.SigninAddressesRequest> addressesRequests;

        account = accountRepository.findByEmail(request.getEmail());

        if (account == null) {
            appRole = applicationRoleRepository.findByRoleName(ApplicationRoles.RESIDENTE).get();
            addressesRequests = request.getAddressesRequests();
            addressSet = new HashSet<>();
            for (SigninRequest.SigninAddressesRequest add: addressesRequests){
                Address accountAddress;
                String complexName = add.getComplex();
                String streetName = add.getStreet();
                Integer streetNumber = Integer.valueOf(add.getNumber());
                accountAddress = addressRepository.findAddressByComplexStreetAndNumber(complexName, streetName, streetNumber);
                if (null != accountAddress){
                    addressSet.add(accountAddress);
                } else {
                    throw new InvitameException(ErrorCodes.DB_ADDRESS_NOT_EXIST);
                }
            }

            if (null != addressSet && !addressSet.isEmpty()) {
                encryptedPassword = encryptionService.encryptString(request.getPassword());
                account = Account.builder()
                        .email(request.getEmail())
                        .password(encryptedPassword)
                        .firstName(request.getName())
                        .lastName(request.getLastName())
                        .phone(request.getPhone())
                        .applicationRole(appRole)
                        .accountNonExpired(Boolean.TRUE)
                        .accountNonLocked(Boolean.TRUE)
                        .credentialsNonExpired(Boolean.TRUE)
                        .enabled(Boolean.FALSE)
                        .accountExpirationDate(Date.valueOf(LocalDate.now().plusYears(1)))
                        .accountEnabledDate(Date.valueOf(LocalDate.now()))
                        .addresses(addressSet)
                        .build();
                account = accountRepository.save(account);
                response = SigninResponse.builder()
                        .fullname(account.getFirstName() + " " + account.getLastName())
                        .email(account.getEmail())
                        .enabled(account.getEnabled())
                        .build();
            } else {
                throw new InvitameException(ErrorCodes.DB_ADDRESS_NOT_EXIST);
            }
        } else {
            throw new InvitameException(ErrorCodes.DB_USERNAME_ALREADY_EXIST);
        }
        return response;
    }


    public AccountDetailsProjection getAccountDetails(String email) throws InvitameException {
        AccountDetailsProjection projection;

        projection = accountRepository.findAccountByEmail(email);
        if (projection != null) {
            return projection;
        } else {
            throw new InvitameException(ErrorCodes.BAD_CREDENTIALS);
        }
    }
}
