package com.app.invitame.api.reponse;

import lombok.*;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ComplexStreetResponse extends ResourceSupport {

    private Long complexId;
    private String complexName;
    private String complexCP;
    private String complexState;
    private String complexMunicipality;
    private List<String> streetNames;

}
