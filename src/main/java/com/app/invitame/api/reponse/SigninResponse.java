package com.app.invitame.api.reponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
public class SigninResponse {

    private String fullname;
    private String email;
    private Boolean enabled;

}
