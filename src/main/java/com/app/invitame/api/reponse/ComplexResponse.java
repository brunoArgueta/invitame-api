package com.app.invitame.api.reponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComplexResponse {

    private Long complexId;
    private String complexName;
    private String complexCP;
    private String complexState;
    private String complexMunicipality;

}
