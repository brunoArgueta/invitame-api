package com.app.invitame.api.reponse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Getter
public class InvitameResponse<B, ERR extends List<? extends InvitameResponseError>> {

    @JsonProperty("body")
    private B body;

    @JsonProperty("errors")
    private ERR  errors;

    @JsonCreator
    public InvitameResponse(B body, ERR errors){
        this.body = body;
        this.errors = errors;
    }

}




