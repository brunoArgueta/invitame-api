package com.app.invitame.api.reponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvitameResponseError {

    private String userMessage;
    private String internalMessage;
    private String code;

}

