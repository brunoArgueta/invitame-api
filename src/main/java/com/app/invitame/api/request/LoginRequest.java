package com.app.invitame.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest implements Serializable {

    @NotEmpty(message = "INVALID EMAIL")
    @Email
    private String email;

    @NotEmpty(message = "INVALID PASSWORD")
    private String password;


}
