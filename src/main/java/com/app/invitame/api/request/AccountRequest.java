package com.app.invitame.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountRequest {

    private Long account_id;

    private String password;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private Boolean accountNonExpired;

    private Boolean accountNonLocked;

    private Boolean credentialsNonExpired;

    private Boolean enabled;

    private Date accountExpirationDate;

    private Date accountLockedDate;

    private Date accountEnabledDate;

    private Set<ApplicationRoleRequest> applicationRoles;

    private Set<AddressRequest> addresses;

}
