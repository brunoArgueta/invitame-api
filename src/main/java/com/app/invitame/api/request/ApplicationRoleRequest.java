package com.app.invitame.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationRoleRequest {

    private Long applicationRoleId;

    private String roleName;

    private String description;

}
