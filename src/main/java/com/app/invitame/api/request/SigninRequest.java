package com.app.invitame.api.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SigninRequest implements Serializable {

    @NotEmpty
    @Pattern(regexp = "^([a-zA-Z\\s]){3,15}$")
    private String name;

    @NotEmpty
    @Pattern(regexp = "^([a-zA-Z\\s]){5,125}$")
    private String lastName;

    @NotEmpty
    @Digits(integer = 10, fraction = 0)
    private String phone;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20}$")
    private String password;

    @NotEmpty
    @NotNull
    List<SigninAddressesRequest> addressesRequests;

    public void addAddressesRequests(@NotEmpty @NotNull SigninAddressesRequest addressesRequest){
        if (null == addressesRequest){
            this.addressesRequests = new ArrayList<>();
        }
        this.addressesRequests.add(addressesRequest);
    }

    @Data
    public static class SigninAddressesRequest {

        @NotEmpty
        @Pattern(regexp = "^([a-zA-Z\\s]){5,50}$")
        private String complex;

        @NotEmpty
        @Pattern(regexp = "^([a-zA-Z\\s]){5,50}$")
        private String street;

        @NotEmpty
        @Digits(integer = 3, fraction = 0)
        private String number;
    }
}


