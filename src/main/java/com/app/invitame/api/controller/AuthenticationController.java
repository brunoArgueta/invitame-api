package com.app.invitame.api.controller;

import com.app.invitame.InvitameException;
import com.app.invitame.api.helper.InvitameAccountHelper;
import com.app.invitame.api.reponse.*;
import com.app.invitame.api.request.LoginRequest;
import com.app.invitame.api.request.SigninRequest;
import com.app.invitame.api.security.JwtTokenProvider;
import com.app.invitame.definition.ErrorCodes;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/invitame/auth/")
@Log4j
@RequiredArgsConstructor
public class AuthenticationController {

    @Autowired
    private InvitameAccountHelper invitameAccountHelper;

    @Autowired
    private AuthenticationManager authenticationManagerBean;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public ResponseEntity login(@Valid @RequestBody LoginRequest data) {
        List<InvitameResponseError> errors = new ArrayList<>();
        LoginResponse body = new LoginResponse();
        InvitameResponse invResponse = new InvitameResponse(body, errors);
        ResponseEntity httpResponse;

        Authentication authentication;
        String email;
        String password;
        try {
            email = data.getEmail();
            password = data.getPassword();
            authentication = new UsernamePasswordAuthenticationToken(email, password);
            authentication = authenticationManagerBean.authenticate(authentication);

            httpResponse = new ResponseEntity<>(invResponse, HttpStatus.OK);
        } catch (DisabledException disableExc) {
            log.error(disableExc.getMessage(), disableExc);

            InvitameResponseError error = InvitameResponseError.builder()
                    .code(ErrorCodes.DISABLED_ACCOUNT.getCode())
                    .internalMessage(disableExc.getMessage())
                    .userMessage(ErrorCodes.DISABLED_ACCOUNT.getMessage())
                    .build();
            errors.add(error);
            httpResponse = new ResponseEntity<>(invResponse, HttpStatus.BAD_REQUEST);
        } catch (LockedException lockedExc) {
            log.error(lockedExc.getMessage(), lockedExc);

            InvitameResponseError error = InvitameResponseError.builder()
                    .code(ErrorCodes.LOCKED_ACCOUNT.getCode())
                    .internalMessage(lockedExc.getMessage())
                    .userMessage(ErrorCodes.LOCKED_ACCOUNT.getMessage())
                    .build();
            errors.add(error);
            httpResponse = new ResponseEntity<>(invResponse, HttpStatus.BAD_REQUEST);
        } catch (BadCredentialsException badCredExc) {
            log.error(badCredExc.getMessage(), badCredExc);

            InvitameResponseError error = InvitameResponseError.builder()
                    .code(ErrorCodes.BAD_CREDENTIALS.getCode())
                    .internalMessage(badCredExc.getMessage())
                    .userMessage(ErrorCodes.BAD_CREDENTIALS.getMessage())
                    .build();
            errors.add(error);
            httpResponse = new ResponseEntity<>(invResponse, HttpStatus.BAD_REQUEST);
        }
        return httpResponse;
    }


    @PostMapping("/signinuser")
    public ResponseEntity signin(@Validated @RequestBody SigninRequest request) {
        List<InvitameResponseError> errors;
        InvitameResponseError error;
        InvitameResponse invResponse;

        SigninResponse response = null;
        errors = new ArrayList<>();
        try {
            response = invitameAccountHelper.createNewResidentAccount(request);

            invResponse = new InvitameResponse(response, errors);

            return new ResponseEntity(invResponse, HttpStatus.CREATED);
        } catch (InvitameException inex) {
            log.error(inex.getError().getMessage(), inex);
            error = InvitameResponseError.builder()
                    .code(inex.getError().getCode())
                    .internalMessage(inex.getError().getMessage())
                    .build();
            errors.add(error);
            invResponse = new InvitameResponse(response, errors);
            return new ResponseEntity(invResponse, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception ex) {
            log.error(ErrorCodes.INVITAME_GENERIC_ERROR.getMessage(), ex);
            error = InvitameResponseError.builder()
                    .code(ErrorCodes.INVITAME_GENERIC_ERROR.getCode())
                    .internalMessage(ErrorCodes.INVITAME_GENERIC_ERROR.getMessage())
                    .build();
            errors.add(error);
            invResponse = new InvitameResponse(response, errors);
            return new ResponseEntity(invResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
