package com.app.invitame.api.controller;


import com.app.invitame.api.helper.ComplexHelper;
import com.app.invitame.api.reponse.ComplexStreetResponse;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.jaxrs.JaxRsLinkBuilder.linkTo;

@RestController
@RequestMapping("/invitame/v2/api/complex")
@Path(value = "/invitame/v2/api/complex")
@Log4j
@RequiredArgsConstructor
public class ComplexController {

    private final ComplexHelper complexHelper;

    @GetMapping(value = "/search/findStreetNameByComplexName", produces = { "application/hal+json" })
    public Resources<ComplexStreetResponse> findStreetNameByComplexName(@RequestParam String complexName){
        ComplexStreetResponse complexStreetResponse = null;
        List<ComplexStreetResponse> complexStreetResponses = null;


        complexStreetResponses = new ArrayList<>();
        complexStreetResponse = complexHelper.getSteetsByComplexName(complexName);
        complexStreetResponses.add(complexStreetResponse);

        complexStreetResponses.forEach(cr -> {
            Long complexId = cr.getComplexId();
            Link selfLink = linkTo(ComplexController.class).slash(complexId).withSelfRel();
            cr.add(selfLink);
        });
        Link link = linkTo(ComplexController.class).withSelfRel();
        Resources<ComplexStreetResponse> result = new Resources<ComplexStreetResponse>(complexStreetResponses, link);
        return result;
    }

}
