package com.app.invitame.api.controller;

import com.app.invitame.api.reponse.InvitameResponseError;
import com.app.invitame.api.reponse.InvitameResponse;
import com.app.invitame.definition.ErrorCodes;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class InvitameErrorController extends AbstractErrorController {

    private List<InvitameResponseError> errors;

    public InvitameErrorController(ErrorAttributes errorAttributes){
        super(errorAttributes);
    }


    @RequestMapping(value = "/error", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity handleError(HttpServletRequest request){
        InvitameResponse invResponse = null;
        List<InvitameResponseError> errors = null;
        InvitameResponseError error = null;
        List<FieldError> errorsList = null;
        FieldError fieldError = null;
        String userMessage = null;

        errors = new ArrayList<>();
        Map<String, Object> errorAttributes = super.getErrorAttributes(request, false);

        if (errorAttributes != null && errorAttributes.containsKey("errors")){
            errorsList = (List) errorAttributes.get("errors");
            if (errorsList != null && errorsList.size() > 0){
                fieldError = errorsList.get(0);
                userMessage = fieldError.getDefaultMessage();
            }

        }

        error = InvitameResponseError.builder()
            .code(ErrorCodes.INVITAME_GENERIC_ERROR.getCode())
            .internalMessage((String)errorAttributes.get("message"))
            .userMessage(userMessage)
            .build();

        errors.add(error);

        invResponse = new InvitameResponse("", errors);
        return new ResponseEntity(invResponse, HttpStatus.BAD_REQUEST);

    }

    @Override
    public String getErrorPath(){
        return "/error";
    }

}
