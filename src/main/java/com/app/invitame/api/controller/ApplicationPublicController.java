package com.app.invitame.api.controller;


import com.app.invitame.InvitameException;
import com.app.invitame.api.helper.ComplexHelper;
import com.app.invitame.api.helper.InvitameAddressHelper;
import com.app.invitame.api.reponse.InvitameResponseError;
import com.app.invitame.api.reponse.InvitameResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/invitame/public")
@Log4j
@RequiredArgsConstructor
public class ApplicationPublicController {

    private final ComplexHelper invitameComplexHelper;

    private final InvitameAddressHelper invitameAddressHelper;

    @GetMapping("/address/findStreetsByComplexName")
    public ResponseEntity getAllStreetsFromComplexId(@RequestParam String complexName) {
        List<InvitameResponseError> errors;
        InvitameResponseError error = null;
        InvitameResponse invResponse = null;

        errors = new ArrayList<>();
        try {

            List<String> steets = invitameAddressHelper.getSteetsByComplexName(complexName);
            invResponse = new InvitameResponse(steets, errors);
            return ResponseEntity.ok(invResponse);
        } catch (InvitameException inex) {
            log.error(inex.getError().getMessage(), inex);
            error = InvitameResponseError.builder()
                    .code(inex.getError().getCode())
                    .internalMessage(inex.getError().getMessage())
                    .build();
            errors.add(error);
            invResponse = new InvitameResponse("", errors);
            return new ResponseEntity(invResponse, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping("/address/findNumbersByComplexNameAndStreetName")
    public ResponseEntity getAllNumberByComplexNameAndStreetName(@RequestParam String streetName,
                                                                 @RequestParam String complexName) {
        List<InvitameResponseError> errors;
        InvitameResponseError error = null;
        InvitameResponse invResponse = null;

        errors = new ArrayList<>();
        try {

            List<Integer> steets = invitameAddressHelper.getNumbersByComplexNameAndStreetName(complexName, streetName);
            invResponse = new InvitameResponse(steets, errors);
            return ResponseEntity.ok(invResponse);
        } catch (InvitameException inex) {
            log.error(inex.getError().getMessage(), inex);
            error = InvitameResponseError.builder()
                    .code(inex.getError().getCode())
                    .internalMessage(inex.getError().getMessage())
                    .build();
            errors.add(error);
            invResponse = new InvitameResponse("", errors);
            return new ResponseEntity(invResponse, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


}
