package com.app.invitame.api.controller;

import com.app.invitame.repository.ApplicationRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;


@RestController
@RequestMapping("/invitame/public/applicationRoles")
@RequiredArgsConstructor
public class ApplicationRoleController {

    private final ApplicationRoleRepository applicationRoleRepository;

    @GetMapping("")
    public HttpEntity getApplicationRoles(){
        return ok(applicationRoleRepository.findAll());
    }

    @GetMapping("/{id}")
    public HttpEntity getApplicationRoles(@PathVariable Long id){
        return ok(applicationRoleRepository.findById(id));
    }
}
