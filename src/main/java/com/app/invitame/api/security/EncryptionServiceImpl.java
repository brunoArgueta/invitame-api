package com.app.invitame.api.security;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EncryptionServiceImpl {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public String encryptString(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    public boolean checkPassword(String plainPassword, String encryptedPassword) {
        return bCryptPasswordEncoder.matches(plainPassword, encryptedPassword);
    }
}
