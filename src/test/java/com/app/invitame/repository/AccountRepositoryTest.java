package com.app.invitame.repository;

import com.app.invitame.definition.ApplicationRoles;
import com.app.invitame.entity.Account;
import com.app.invitame.entity.Address;
import com.app.invitame.entity.ApplicationRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AccountRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AddressRepository addressRepository;

    @Test
    public void it_should_save_account() {
        Account account;
        Account findAccount;
        Address address;
        ApplicationRole role;

        role = ApplicationRole
                .builder()
                .roleName(ApplicationRoles.RESIDENTE)
                .build();

        address = addressRepository.findAddressByComplexStreetAndNumber("PUNTA DEL CARMEN", "CIRCUITO AKUMAL", 105);

        account = Account.builder()
                .email("bruno.argueta@gmail.com")
                .password("encryptedPassword")
                .firstName("Bruno")
                .lastName("Argueta Montes de Oca")
                .phone("3319186410")
                .applicationRole(role)
                .accountNonExpired(Boolean.TRUE)
                .accountNonLocked(Boolean.TRUE)
                .credentialsNonExpired(Boolean.TRUE)
                .enabled(Boolean.FALSE)
                .accountExpirationDate(Date.valueOf(LocalDate.now().plusYears(1)))
                .accountEnabledDate(Date.valueOf(LocalDate.now()))
                .build();

        account.addAddress(address);
        account = entityManager.persist(account);
        findAccount = accountRepository.findById(account.getAccountId()).get();
        assertEquals(findAccount.getEmail(), account.getEmail());
    }

}