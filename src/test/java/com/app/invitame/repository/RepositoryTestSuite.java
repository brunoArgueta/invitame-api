package com.app.invitame.repository;


import com.app.invitame.api.helper.InvitameAccountHelperTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvitameAccountHelperTest.class,
        AccountRepositoryTest.class})
public class RepositoryTestSuite {

}
