package com.app.invitame.api.helper;

import com.app.invitame.InvitameException;
import com.app.invitame.api.reponse.SigninResponse;
import com.app.invitame.api.request.SigninRequest;
import com.app.invitame.definition.ApplicationRoles;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class InvitameAccountHelperTest {

    @Autowired
    private InvitameAccountHelper invitameAccountHelper;

    @Test
    public void createNewResidentAccountTest() throws InvitameException {
        SigninRequest request;
        SigninRequest.SigninAddressesRequest addressesRequest1;
        SigninRequest.SigninAddressesRequest addressesRequest2;
        SigninResponse response;

        addressesRequest1 = new SigninRequest.SigninAddressesRequest();
        addressesRequest1.setComplex("PUNTA DEL CARMEN");
        addressesRequest1.setNumber("105");
        addressesRequest1.setStreet("CIRCUITO AKUMAL");

        addressesRequest2 = new SigninRequest.SigninAddressesRequest();
        addressesRequest2.setComplex("PUNTA DEL CARMEN");
        addressesRequest2.setNumber("107");
        addressesRequest2.setStreet("CIRCUITO AKUMAL");

        List<SigninRequest.SigninAddressesRequest> addressesRequestList = new ArrayList<>();
        addressesRequestList.add(addressesRequest1);
        addressesRequestList.add(addressesRequest2);

        request = SigninRequest.builder()
                .phone("3319186410")
                .password("ENCRIPTED PASSWORD")
                .name("BRUNO")
                .lastName("ARGUETA MONTES DE OCA")
                .email("bruno.argueta2@gmail.com")
                .addressesRequests(addressesRequestList)
                .build();

        response = invitameAccountHelper.createNewResidentAccount(request);
        assertTrue(response.getFullname().equals("BRUNO ARGUETA MONTES DE OCA"));
    }

    @Test
    public void createNewResidentAccount_WITH_DB_USERNAME_ALREADY_EXIST() throws InvitameException {
        SigninRequest request;
        SigninRequest.SigninAddressesRequest addressesRequest1;
        SigninRequest.SigninAddressesRequest addressesRequest2;
        SigninResponse response;

        addressesRequest1 = new SigninRequest.SigninAddressesRequest();
        addressesRequest1.setComplex("PUNTA DEL CARMEN");
        addressesRequest1.setNumber("105");
        addressesRequest1.setStreet("CIRCUITO AKUMAL");

        List<SigninRequest.SigninAddressesRequest> addressesRequestList = new ArrayList<>();
        addressesRequestList.add(addressesRequest1);

        request = SigninRequest.builder()
                .phone("3319186410")
                .password("ENCRIPTED PASSWORD")
                .name("BRUNO")
                .lastName("ARGUETA MONTES DE OCA")
                .email("bruno.argueta1@gmail.com")
                .addressesRequests(addressesRequestList)
                .build();

        invitameAccountHelper.createNewResidentAccount(request);
        assertThrows(InvitameException.class, () -> invitameAccountHelper.createNewResidentAccount(request));
    }

    @Test
    public void createNewResidentAccount_WITH_DB_ADDRESS_NOT_EXIST() throws InvitameException {
        SigninRequest request;
        SigninRequest.SigninAddressesRequest addressesRequest1;
        SigninResponse response;

        addressesRequest1 = new SigninRequest.SigninAddressesRequest();
        addressesRequest1.setComplex("PUNTA DEL CARMEN");
        addressesRequest1.setNumber("105");
        addressesRequest1.setStreet("CIRCUITO AKUMAL");

        List<SigninRequest.SigninAddressesRequest> addressesRequestList = new ArrayList<>();
        addressesRequestList.add(addressesRequest1);

        request = SigninRequest.builder()
                .phone("3319186410")
                .password("ENCRIPTED PASSWORD")
                .name("BRUNO")
                .lastName("ARGUETA MONTES DE OCA")
                .email("bruno.argueta1@gmail.com")
                .addressesRequests(addressesRequestList)
                .build();

        assertThrows(InvitameException.class, () -> invitameAccountHelper.createNewResidentAccount(request));
    }

}