package com.app.invitame.api.controller;

import com.app.invitame.api.request.LoginRequest;
import com.app.invitame.api.request.SigninRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthenticationControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void signin() throws Exception {
        HttpHeaders headers;
        HttpEntity<SigninRequest> request;
        ResponseEntity<String> response;
        String url = createURLWithPort("/invitame/auth/signinuser");

        SigninRequest.SigninAddressesRequest addressesRequest1 = new SigninRequest.SigninAddressesRequest();
        addressesRequest1.setComplex("PUNTA DEL CARMEN");
        addressesRequest1.setNumber("105");
        addressesRequest1.setStreet("CIRCUITO AKUMAL");

        SigninRequest.SigninAddressesRequest addressesRequest2 = new SigninRequest.SigninAddressesRequest();
        addressesRequest2.setComplex("PUNTA DEL CARMEN");
        addressesRequest2.setNumber("107");
        addressesRequest2.setStreet("CIRCUITO AKUMAL");

        List<SigninRequest.SigninAddressesRequest> addresses = new ArrayList<>();
        addresses.add(addressesRequest1);
        addresses.add(addressesRequest2);

        SigninRequest signinRequest = SigninRequest.builder()
                .email("bruno.argueta@gmail.com")
                .lastName("Argueta Montes de Oca")
                .name("Bruno")
                .password("Bruno1136@")
                .phone("3319186410")
                .addressesRequests(addresses)
                .build();
        headers = new HttpHeaders();
        request = new HttpEntity<>(signinRequest, headers);

        response = restTemplate.postForEntity(url, request, String.class);
        assertTrue(response.getStatusCode().value() ==  HttpStatus.CREATED.value());
        assertTrue(response.getBody().contains("Bruno Argueta Montes de Oca"));
    }

    @Test
    public void login_with_disabled_user() throws Exception {
        HttpHeaders headers;
        HttpEntity<SigninRequest> request;
        ResponseEntity<String> response;
        String url = createURLWithPort("/invitame/auth/login");

        LoginRequest loginRequest = LoginRequest.builder()
                .email("bruno.argueta@gmail.com")
                .password("Bruno1136@")
                .build();
        headers = new HttpHeaders();
        request = new HttpEntity(loginRequest, headers);
        response = restTemplate.postForEntity(url, request, String.class);
        assertTrue(response.getStatusCode().value() ==  HttpStatus.BAD_REQUEST.value());
    }

    private String createURLWithPort(String uri){
        return "http://localhost:" + port + uri;
    }
}